const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

let heroesList = [
    { id: 11, name: 'Mr. Nice' },
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' },
    { id: 20, name: 'Tornado' }
];

var app = express();
app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// get all
app.get('/api/heroes', (req, res) => {
    res.json(heroesList)
});

// get one
app.get('/api/heroes/:id', (req, res) => {
    console.log('aa' == 'Aa');

    let hero = heroesList.filter(hero => hero.id === +req.params.id);
    res.json(hero[0]);
    // hero = JSON.stringify(hero[0]);
    // res.send(hero);
});

// update
app.put('/api/heroes', (req, res, next) => {
    for (let i = 0; i < heroesList.length; i++) {
        if (heroesList[i].id === +req.body.id && heroesList[i].name !== req.body.name) {
            heroesList[i].name = req.body.name;
        }
    }
    res.json({ status: 'updated hero' });
});

// add new - or search
app.post('/api/heroes', (req, res) => {

    let i;
    for (i = 0; i < heroesList.length; i++) {
        if (i === (heroesList.length - 1)) {
            req.body.id = heroesList[i].id + 1;
        }
    }
    heroesList.push(req.body);
    res.json(heroesList[i]);
});

// delete
app.delete('/api/heroes/:id', (req, res) => {
    let newArr = [];
    for (let i = 0; i < heroesList.length; i++) {
        if (heroesList[i].id !== +req.params.id) {
            newArr.push(heroesList[i]);
        }
    }
    heroesList = newArr;
    res.json(heroesList);
});

// search
app.get('/api/search/', (req, res) => {
    let name = req.query.name,
        arr = [],
        length = name.length,
        i = 0;
    for (i; i < heroesList.length; i++) {
        let str = heroesList[i].name.split(' ');
        str[0] = str[0].slice(0, length);
        if (str[1]) { str[1] = str[1].slice(0, length); }
        if (name == str[0] || name == str[1]) {
            arr.push(heroesList[i]);
        }
    }
    res.json(arr);
});

app.listen(3000)